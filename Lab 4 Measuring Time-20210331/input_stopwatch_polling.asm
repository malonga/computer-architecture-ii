.equ    RAM, 0x1000
.equ    LEDs, 0x2000
.equ    TIMER, 0x2020
.equ    BUTTON, 0x2030

.equ    LFSR, RAM

; Variable initialization for spend_time
addi t0, zero, 18
stw t0, LFSR(zero)

; <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
; DO NOT CHANGE ANYTHING ABOVE THIS LINE
; <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

addi sp, zero, LEDs

;; USER INPUT CASE
addi s0, s0, 0 ; s0 is the timer, initialize with value 0
addi a0, s0, 0
call display
;100 ms at 50MHz -> 5'000'000 cycles 

; We want to measure time between pushes with 5'000'0000 cycles accuracy
; So each polling should be spaced out at most by 2'500'000 to have that guarantee
; let's say less than 1'250'000 in our case
; Each increment should be spaced out by 5'000'000 cycles

; Polling loop :
; n is the initital value put into t0 before the loop
; 1'249'993 >= 7(n+1) + 4*(3/5) + 3*(5) + 0/spend_time = 7(n) + 34/(42+spend_time)
;---------- Assuming spend_time = 0 --------
; n = 178564 <=> poll_loop = 1249988 (1249982/1249990)
; 178564 = 32 * 5580 + 7

; Increment loop :
; m is value being loaded into t2
; 5'000'000 = 4 * polling_loop + 3*(4) + 4*(2) + (m+1)*(7) + display = 4 * polling_loop + 27 + m*7 + display = 4999979 + m*7 + display
; --- Assuming display = 0 ------------------
; 5'000'000-4'999'979 = 21 = 7*3 

addi s1, zero, 5580  ; t0 -> period of the polling process in cycles
slli s1, s1, 5 ; t0 -> period of the polling process in cycles, commented when simulating in nios2sim
addi s1, s1, 7 ; t0 = period of our polling process in cycles
addi s2, s2, 3; t1 -> floor of the period of our increment_process by polling_process period
increment_loop:
poll_loop:
addi s1, s1, -1 ; decrement t0
bne s1, zero, poll_loop ; keep doing so until we reach 0
poll_buttons:
ldw s1, BUTTON+4(zero) ; read edge capture
andi s1, s1, 1 ; t0 = edge_capture of b0
beq s1, zero, b0_not_pressed
call spend_time
stw zero, BUTTON+4(zero) ; clear edge_capture
b0_not_pressed:
addi s2, s2, -1
addi s1, zero, 5580; prepare t0 for another polling countdown
slli s1, s1, 5 ; commented when simulating in nios2sim
addi s1, s1, 7
bne s2, zero, poll_loop
increment_timer:
addi s0, s0, 1 ; add 1 to our internal register representing timer
addi a0, s0, 0 ; Pass value of timer as argument to display
call display ; display
addi s3, zero, 3
increment_stall_loop:
addi s3, s3, -1
bne s3, zero, increment_stall_loop
addi s2, zero, 3; Prepare t1 for another increment countdown
beq zero, zero, increment_loop


; Conclusion : 
; 1. Since the timer is incremented by the processor, it may not be incremented while spend_time executes. The execution time of spend_time can be significantly long
; 2.a It isn't possible to estimate the execution time of spend_time. Hence it is impossible to know how long the processor is busy executing spend_time (complexity of spend_time known only at runtime)
; 2.b It isn't possible to modify the main such that the execution time of the rest of the loop is increased/decreased to compensate for spend_time
; 3. It is therefore impossible to guarantee correct timing while allowing for calls to function of which the execution time is unknown at programming time let alone runtime.

; WRITE YOUR CODE AND CONSTANT DEFINITIONS HERE

; <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
; DO NOT CHANGE ANYTHING BELOW THIS LINE
; <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
; ----------------- Common functions --------------------
; a0 = tenths of second
display:
    addi   sp, sp, -20
    stw    ra, 0(sp)
    stw    s0, 4(sp)
    stw    s1, 8(sp)
    stw    s2, 12(sp)
    stw    s3, 16(sp)
    add    s0, a0, zero
    add    a0, zero, s0
    addi   a1, zero, 600
    call   divide
    add    s0, zero, v0
    add    a0, zero, v1
    addi   a1, zero, 100
    call   divide
    add    s1, zero, v0
    add    a0, zero, v1
    addi   a1, zero, 10
    call   divide
    add    s2, zero, v0
    add    s3, zero, v1

    slli   s3, s3, 2
    slli   s2, s2, 2
    slli   s1, s1, 2
    ldw    s3, font_data(s3)
    ldw    s2, font_data(s2)
    ldw    s1, font_data(s1)

    xori   t4, zero, 0x8000
    slli   t4, t4, 16
    add    t5, zero, zero
    addi   t6, zero, 4
    minute_loop_s3:
    beq    zero, s0, minute_end
    beq    t6, t5, minute_s2
    or     s3, s3, t4
    srli   t4, t4, 8
    addi   s0, s0, -1
    addi   t5, t5, 1
    br minute_loop_s3

    minute_s2:
    xori   t4, zero, 0x8000
    slli   t4, t4, 16
    add    t5, zero, zero
    minute_loop_s2:
    beq    zero, s0, minute_end
    beq    t6, t5, minute_s1
    or     s2, s2, t4
    srli   t4, t4, 8
    addi   s0, s0, -1
    addi   t5, t5, 1
    br minute_loop_s2

    minute_s1:
    xori   t4, zero, 0x8000
    slli   t4, t4, 16
    add    t5, zero, zero
    minute_loop_s1:
    beq    zero, s0, minute_end
    beq    t6, t5, minute_end
    or     s1, s1, t4
    srli   t4, t4, 8
    addi   s0, s0, -1
    addi   t5, t5, 1
    br minute_loop_s1

    minute_end:
    stw    s1, LEDs(zero)
    stw    s2, LEDs+4(zero)
    stw    s3, LEDs+8(zero)

    ldw    ra, 0(sp)
    ldw    s0, 4(sp)
    ldw    s1, 8(sp)
    ldw    s2, 12(sp)
    ldw    s3, 16(sp)
    addi   sp, sp, 20

    ret

flip_leds:
    addi t0, zero, -1
    ldw t1, LEDs(zero)
    xor t1, t1, t0
    stw t1, LEDs(zero)
    ldw t1, LEDs+4(zero)
    xor t1, t1, t0
    stw t1, LEDs+4(zero)
    ldw t1, LEDs+8(zero)
    xor t1, t1, t0
    stw t1, LEDs+8(zero)
    ret

spend_time:
    addi sp, sp, -4
    stw  ra, 0(sp)
    call flip_leds
    ldw t1, LFSR(zero)
    add t0, zero, t1
    srli t1, t1, 2
    xor t0, t0, t1
    srli t1, t1, 1
    xor t0, t0, t1
    srli t1, t1, 1
    xor t0, t0, t1
    andi t0, t0, 1
    slli t0, t0, 7
    srli t1, t1, 1
    or t1, t0, t1
    stw t1, LFSR(zero)
    slli t1, t1, 15
    addi t0, zero, 1
    slli t0, t0, 22
    add t1, t0, t1

spend_time_loop:
    addi   t1, t1, -1
    bne    t1, zero, spend_time_loop
    
    call flip_leds
    ldw ra, 0(sp)
    addi sp, sp, 4

    ret

; v0 = a0 / a1
; v1 = a0 % a1
divide:
    add    v0, zero, zero
divide_body:
    add    v1, a0, zero
    blt    a0, a1, end
    sub    a0, a0, a1
    addi   v0, v0, 1
    br     divide_body
end:
    ret



font_data:
    .word 0x7E427E00 ; 0
    .word 0x407E4400 ; 1
    .word 0x4E4A7A00 ; 2
    .word 0x7E4A4200 ; 3
    .word 0x7E080E00 ; 4
    .word 0x7A4A4E00 ; 5
    .word 0x7A4A7E00 ; 6
    .word 0x7E020600 ; 7
    .word 0x7E4A7E00 ; 8
    .word 0x7E4A4E00 ; 9
    .word 0x7E127E00 ; A
    .word 0x344A7E00 ; B
    .word 0x42423C00 ; C
    .word 0x3C427E00 ; D
    .word 0x424A7E00 ; E
    .word 0x020A7E00 ; F
