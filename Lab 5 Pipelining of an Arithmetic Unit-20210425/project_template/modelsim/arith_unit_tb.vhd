library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity arith_unit_tb is
end;

architecture bench of arith_unit_tb is
    signal clk     : std_logic;
    signal reset_n : std_logic;
    signal start   : std_logic;
    signal sel     : std_logic;
    signal A, B, C : unsigned(7 downto 0);
    signal D       : unsigned(31 downto 0);
    signal done    : std_logic;
    

    component arith_unit is
        port(
        clk     : in  std_logic;
        reset_n : in  std_logic;
        start   : in  std_logic;
        sel     : in  std_logic;
        A, B, C : in  unsigned(7 downto 0);
        D       : out unsigned(31 downto 0);
        done    : out std_logic
    );
    end component;


begin
    combinatorial_arith_unit : arith_unit port map(
            clk => clk,
            reset_n => reset_n,
            start => start,
		sel => sel,
		A => A,
		B => B,
		C => C,
		D => D,
		done => done
        );
    
    process
        variable err         : boolean := false;
        variable line_output : line;
	variable res : integer := 0;
	variable resu : unsigned(31 downto 0);
    begin
	A <= to_unsigned(1,8);
	B <= to_unsigned(255,8);
	C <= to_unsigned(1,8);
	sel <= '0';
	wait for 40 ns;
	res := (((to_integer(b)*to_integer(c))*(to_integer(a)+to_integer(b)))+(to_integer(b)*to_integer(b)*to_integer(c)*to_integer(c)));
	if (D /= res and not err) then
                    err := true;
                    report "arith_unit combinatorial, res not matching! A= 1 B= 255 C= 1 and sel='0' but D = " & integer'image(to_integer(D)) severity ERROR;
        else 
		report "individual test for sel0 successfull" severity note;
	end if;

        for a_in in 0 to 255 loop
	for b_in in 0 to 255 loop
            for c_in in 0 to 255 loop
                --A <= to_unsigned(i, 8);
                --B <= to_unsigned(j, 8);
		A <= to_unsigned(a_in, 8);
		B <= to_unsigned(b_in, 8);
		C <= to_unsigned(c_in, 8);
		sel <= '1' ;
                wait for 5 ns;
		resu := ((2*a+b)*(2*a+b)+a*a*a*a);
                --if (P /= (i * j) and not err) then
                --    err := true;
                --    report "not matching! A= " & integer'image(i) &" B= " &integer'image(j) &" and P = " & integer'image(to_integer(P)) severity ERROR;
                --end if;
		if (D /= resu) then
                    err := true;
                    report "arith_unit combinatorial, res not matching! A= " & integer'image(a_in) &" B= " &integer'image(b_in)&" C= " &integer'image(c_in) &" and sel='1' but D = " & integer'image(to_integer(D))&" instead of : " &integer'image(res) severity ERROR;
                end if;
		sel <= '0';
		wait for 5 ns;
		resu := (((b*c)*(a+b))+(b*b*c*c));
		if (D /= ((b*c)*(a+b)+b*b*c*c)) then
                    err := true;
                    report "arith_unit combinatorial, res not matching! A= " & integer'image(a_in) &" B= " &integer'image(b_in)&" C= " &integer'image(c_in) &"and sel='0' but D = " & integer'image(to_integer(D)) severity ERROR;
                end if;
            end loop;
        end loop;
	end loop;

        line_output := new string'("===================================================================");
        writeline(output, line_output);
        if (err) then
            line_output := new string'("Errors encountered during simulation.");
        else
            line_output := new string'("Simulation is successful");
        end if;
        writeline(output, line_output);
        line_output := new string'("===================================================================");
        writeline(output, line_output);

        wait;
    end process;

end bench;
