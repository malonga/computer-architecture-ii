library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_textio.all;
use std.textio.all;

entity multiplier_tb is
end;

architecture bench of multiplier_tb is
    signal A, B : unsigned(7 downto 0);
    signal P    : unsigned(15 downto 0);

    signal A16, B16 : unsigned(15 downto 0);
    signal P16      : unsigned(31 downto 0);

    component multiplier is
        port(
            A, B : in  unsigned(7 downto 0);
            P    : out unsigned(15 downto 0)
        );
    end component;

component multiplier16 is
        port(
            A, B : in  unsigned(15 downto 0);
            P    : out unsigned(31 downto 0)
        );
    end component;

begin
    multiplier_0 : multiplier port map(
            A => A,
            B => B,
            P => P
        );
    multiplier16_0 : multiplier16 port map(
	    A => A16,
	    B => B16,
	    P => P16
	);

    process
        variable err         : boolean := false;
        variable line_output : line;
    begin
        for i in 0 to 65535 loop
            for j in 0 to 65535 loop
                --A <= to_unsigned(i, 8);
                --B <= to_unsigned(j, 8);
		A16 <= to_unsigned(i, 16);
		B16 <= to_unsigned(j, 16);
                wait for 5 ns;
                --if (P /= (i * j) and not err) then
                --    err := true;
                --    report "not matching! A= " & integer'image(i) &" B= " &integer'image(j) &" and P = " & integer'image(to_integer(P)) severity ERROR;
                --end if;
		if (P16 /= (i * j) and not err) then
                    err := true;
                    report "mul_16 not matching! A= " & integer'image(i) &" B= " &integer'image(j) &" and P = " & integer'image(to_integer(P16)) severity ERROR;
                end if;
            end loop;
        end loop;

        line_output := new string'("===================================================================");
        writeline(output, line_output);
        if (err) then
            line_output := new string'("Errors encountered during simulation.");
        else
            line_output := new string'("Simulation is successful");
        end if;
        writeline(output, line_output);
        line_output := new string'("===================================================================");
        writeline(output, line_output);

        wait;
    end process;

end bench;
