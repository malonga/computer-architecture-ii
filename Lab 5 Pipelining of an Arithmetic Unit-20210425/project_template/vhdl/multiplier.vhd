-- =============================================================================
-- ================================= multiplier ================================
-- =============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity multiplier is
    port(
        A, B : in  unsigned(7 downto 0);
        P    : out unsigned(15 downto 0)
    );
end multiplier;

architecture combinatorial of multiplier is
subtype PARTIALSUM is unsigned(15 downto 0);
signal part_sum00 : PARTIALSUM :=(others=>'0');
signal part_sum01 : PARTIALSUM :=(others=>'0');
signal part_sum02 : PARTIALSUM :=(others=>'0');
signal part_sum03 : PARTIALSUM :=(others=>'0');
signal part_sum10 : PARTIALSUM :=(others=>'0');
signal part_sum11 : PARTIALSUM :=(others=>'0');
signal part_sum20 : PARTIALSUM :=(others=>'0');

subtype PARTIALPROD is unsigned(7 downto 0);
signal part_prod0 : PARTIALPROD :=(others=>'0');
signal part_prod1 : PARTIALPROD :=(others=>'0');
signal part_prod2 : PARTIALPROD :=(others=>'0');
signal part_prod3 : PARTIALPROD :=(others=>'0');
signal part_prod4 : PARTIALPROD :=(others=>'0');
signal part_prod5 : PARTIALPROD :=(others=>'0');
signal part_prod6 : PARTIALPROD :=(others=>'0');
signal part_prod7 : PARTIALPROD :=(others=>'0');

begin
part_prod0 <= B when (A(0)='1') else (others => '0');
part_prod1 <= B when (A(1)='1') else (others => '0');
part_prod2 <= B when (A(2)='1') else (others => '0');
part_prod3 <= B when (A(3)='1') else (others => '0');
part_prod4 <= B when (A(4)='1') else (others => '0');
part_prod5 <= B when (A(5)='1') else (others => '0');
part_prod6 <= B when (A(6)='1') else (others => '0');
part_prod7 <= B when (A(7)='1') else (others => '0');


part_sum00<=("00000000"&part_prod0) + ("0000000"&part_prod1&"0");
part_sum01<=("00000000"&part_prod2) + ("0000000"&part_prod3&"0");
part_sum02<=("00000000"&part_prod4) + ("0000000"&part_prod5&"0");
part_sum03<=("00000000"&part_prod6) + ("0000000"&part_prod7&"0");


part_sum10<=part_sum00 + (part_sum01(13 downto 0)&"00");
part_sum11<=part_sum02 + (part_sum03(13 downto 0)&"00");

part_sum20<=part_sum10 + (part_sum11(11 downto 0)&"0000");

P<=part_sum20;
end combinatorial;

-- =============================================================================
-- =============================== multiplier16 ================================
-- =============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity multiplier16 is
    port(
        A, B : in  unsigned(15 downto 0);
        P    : out unsigned(31 downto 0)
    );
end multiplier16;

architecture combinatorial of multiplier16 is

	signal low_mul_res : unsigned(15 downto 0) := (others => '0');
	signal mid_mul0_res : unsigned(15 downto 0) := (others => '0');
	signal mid_mul1_res : unsigned(15 downto 0) := (others => '0');
	signal high_mul_res : unsigned(15 downto 0) := (others => '0');
    -- 8-bit multiplier component declaration
	component multiplier
        port(
            A, B : in  unsigned(7 downto 0);
            P    : out unsigned(15 downto 0)
        );
	end component;

	

begin
	multiplier_low : multiplier port map(
            A => A(7 downto 0),
            B => B(7 downto 0),
            P => low_mul_res
        );
	multiplier_mid0 : multiplier port map(
            A => A(7 downto 0),
            B => B(15 downto 8),
            P => mid_mul0_res
        );
	multiplier_mid1 : multiplier port map(
            A => A(15 downto 8),
            B => B(7 downto 0),
            P => mid_mul1_res
        );

	multiplier_high : multiplier port map(
            A => A(15 downto 8),
            B => B(15 downto 8),
            P => high_mul_res(15 downto 0)
        );

	P<=("0000000000000000"&low_mul_res)+("00000000"&mid_mul0_res&("00000000"))+
	("00000000"&mid_mul1_res&("00000000"))+(high_mul_res&"0000000000000000");
end combinatorial;

-- =============================================================================
-- =========================== multiplier16_pipeline ===========================
-- =============================================================================

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity multiplier16_pipeline is
    port(
        clk     : in  std_logic;
        reset_n : in  std_logic;
        A, B    : in  unsigned(15 downto 0);
        P       : out unsigned(31 downto 0)
    );
end multiplier16_pipeline;

architecture pipeline of multiplier16_pipeline is

	signal low_mul_res : unsigned(15 downto 0) := (others => '0');
	signal mid_mul0_res : unsigned(15 downto 0) := (others => '0');
	signal mid_mul1_res : unsigned(15 downto 0) := (others => '0');
	signal high_mul_res : unsigned(15 downto 0) := (others => '0');
	signal s_P : unsigned(31 downto 0) := (others => '0');
    -- 8-bit multiplier component declaration
	component multiplier
        port(
            A, B : in  unsigned(7 downto 0);
            P    : out unsigned(15 downto 0)
        );
	end component;

	

begin
	multiplier_low : multiplier port map(
            A => A(7 downto 0),
            B => B(7 downto 0),
            P => low_mul_res
        );
	multiplier_mid0 : multiplier port map(
            A => A(7 downto 0),
            B => B(15 downto 8),
            P => mid_mul0_res
        );
	multiplier_mid1 : multiplier port map(
            A => A(15 downto 8),
            B => B(7 downto 0),
            P => mid_mul1_res
        );

	multiplier_high : multiplier port map(
            A => A(15 downto 8),
            B => B(15 downto 8),
            P => high_mul_res(15 downto 0)
        );

	update : process(clk, reset_n)
		begin
		if (reset_n ='0') then
			-- reset
			s_P<=(others => '0');
		elsif (rising_edge(clk)) then
			s_P<=("0000000000000000"&low_mul_res)+("00000000"&mid_mul0_res&("00000000"))+
			("00000000"&mid_mul1_res&("00000000"))+(high_mul_res&"0000000000000000");

		end if;
	end process;
	P<=s_P;
end pipeline;
