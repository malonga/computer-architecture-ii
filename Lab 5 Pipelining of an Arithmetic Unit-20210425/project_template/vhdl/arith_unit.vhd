library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity arith_unit is
    port(
        clk     : in  std_logic;
        reset_n : in  std_logic;
        start   : in  std_logic;
        sel     : in  std_logic;
        A, B, C : in  unsigned(7 downto 0);
        D       : out unsigned(31 downto 0);
        done    : out std_logic
    );
end arith_unit;

-- =============================================================================
-- =============================== COMBINATORIAL ===============================
-- =============================================================================

-- Up to 62,5 MHz
architecture combinatorial of arith_unit is
    component multiplier
        port(
            A, B : in  unsigned(7 downto 0);
            P    : out unsigned(15 downto 0)
        );
    end component;

    component multiplier16
        port(
            A, B : in  unsigned(15 downto 0);
            P    : out unsigned(31 downto 0)
        );
    end component;

    signal s_in_A0_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_A0_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_A0 : unsigned(15 downto 0) := (others => '0');

    signal s_in_A1_a : unsigned(31 downto 0) := (others => '0');
    signal s_in_A1_b : unsigned(31 downto 0) := (others => '0');
    signal s_out_A1 : unsigned(31 downto 0) := (others => '0');

    signal s_in_M1_a : unsigned(7 downto 0) := (others => '0');
    signal s_in_M1_b : unsigned(7 downto 0) := (others => '0');
    signal s_out_M1 : unsigned(15 downto 0) := (others => '0');

    signal s_in_M2_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_M2_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_M2 : unsigned(31 downto 0) := (others => '0');

    signal s_in_M0_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_M0_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_M0 : unsigned(31 downto 0) := (others => '0');

begin


	multiplier0 : multiplier16 port map(
		A => s_in_M0_a,
		B => s_in_M0_b,
		P => s_out_M0
	); 

	multiplier1 : multiplier port map(
		A => s_in_M1_a,
		B => s_in_M1_b,
		P => s_out_M1
	); 

	multiplier2 : multiplier16 port map(
		A => s_in_M2_a,
		B => s_in_M2_b,
		P => s_out_M2
	); 

s_in_A0_a <= ("0000000"&A(7 downto 0)&"0") when (sel='1') else ("00000000"&A) ;
s_in_A0_b <= "00000000"&B;
s_out_A0 <= s_in_A0_a + s_in_A0_b; -- (a+b;2a+b)

s_in_M1_a <= A when (sel='1') else B;
s_in_M1_b <= A when (sel='1') else C;
-- s_out_M1_b (b.c;a.a)

s_in_M0_a <= s_out_A0;
s_in_M0_b <= s_out_A0 when (sel='1') else s_out_M1;
-- s_out_M0 ((a+b).(b.c);(2a+b).(2a+b))

s_in_M2_a <= s_out_M1;
s_in_M2_b <= s_out_M1;
-- s_out_M2 ((b.c).(b.c);(a.a).(a.a))

s_in_A1_a <= s_out_M0;
s_in_A1_b <= s_out_M2;
s_out_A1 <= s_in_A1_a + s_in_A1_b;
-- s_out_A1 ( (a+b).(b.c)+(b.c).(b.c);(2a+b).(2a+b) +(a.a).(a.a))

D<=s_out_A1;

done<=start;

end combinatorial;

-- =============================================================================
-- ============================= 1 STAGE PIPELINE ==============================
-- =============================================================================

-- intermidiary register before M0 and M2
-- up to 80 MHz

architecture one_stage_pipeline of arith_unit is
    component multiplier
        port(
            A, B : in  unsigned(7 downto 0);
            P    : out unsigned(15 downto 0)
        );
    end component;

    component multiplier16
        port(
            A, B : in  unsigned(15 downto 0);
            P    : out unsigned(31 downto 0)
        );
    end component;
    
    signal s_done : std_logic;
    
    signal s_to_be_in_M0_a : unsigned(15 downto 0) := (others => '0');
    signal s_to_be_in_M0_b : unsigned(15 downto 0) := (others => '0');
    signal s_to_be_in_M2 : unsigned(15 downto 0) := (others => '0');

    signal s_in_A0_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_A0_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_A0 : unsigned(15 downto 0) := (others => '0');

    signal s_in_A1_a : unsigned(31 downto 0) := (others => '0');
    signal s_in_A1_b : unsigned(31 downto 0) := (others => '0');
    signal s_out_A1 : unsigned(31 downto 0) := (others => '0');

    signal s_in_M1_a : unsigned(7 downto 0) := (others => '0');
    signal s_in_M1_b : unsigned(7 downto 0) := (others => '0');
    signal s_out_M1 : unsigned(15 downto 0) := (others => '0');

    signal s_in_M2_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_M2_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_M2 : unsigned(31 downto 0) := (others => '0');


    signal s_in_M0_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_M0_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_M0 : unsigned(31 downto 0) := (others => '0');

begin


	multiplier0 : multiplier16 port map(
		A => s_in_M0_a,
		B => s_in_M0_b,
		P => s_out_M0
	); 

	multiplier1 : multiplier port map(
		A => s_in_M1_a,
		B => s_in_M1_b,
		P => s_out_M1
	); 

	multiplier2 : multiplier16 port map(
		A => s_in_M2_a,
		B => s_in_M2_b,
		P => s_out_M2
	); 

update : process(clk, reset_n)
	begin
	if (reset_n='0') then
		s_done<='0';
	elsif(rising_edge(clk)) then
		s_done <= start;
		if (start = '1') then 
			-- change value the second part is working with
			s_in_M0_a <= s_to_be_in_M0_a;
			s_in_M0_b <= s_to_be_in_M0_b;
			s_in_M2_a <= s_to_be_in_M2;
			s_in_M2_b <= s_to_be_in_M2;
		end if;
	end if;
end process;

s_in_A0_a <= ("0000000"&A(7 downto 0)&"0") when (sel='1') else ("00000000"&A) ;
s_in_A0_b <= "00000000"&B;
s_out_A0 <= s_in_A0_a + s_in_A0_b; -- (a+b;2a+b)

s_in_M1_a <= A when (sel='1') else B;
s_in_M1_b <= A when (sel='1') else C;
-- s_out_M1_b (b.c;a.a)

s_to_be_in_M0_a <= s_out_A0;
s_to_be_in_M0_b <= s_out_A0 when (sel='1') else s_out_M1;
-- s_out_M0 ((a+b).(b.c);(2a+b).(2a+b))

s_to_be_in_M2 <= s_out_M1;
-- s_out_M2 ((b.c).(b.c);(a.a).(a.a))

s_in_A1_a <= s_out_M0;
s_in_A1_b <= s_out_M2;
s_out_A1 <= s_in_A1_a + s_in_A1_b;
-- s_out_A1 ( (a+b).(b.c)+(b.c).(b.c);(2a+b).(2a+b) +(a.a).(a.a))

D<=s_out_A1;

done<=s_done;



end one_stage_pipeline;

-- =============================================================================
-- ============================ 2 STAGE PIPELINE I =============================
-- =============================================================================

-- up to 93 MHz
-- intermediary stage before M0 and M2
-- intermediary stage after M0 and M2

architecture two_stage_pipeline_1 of arith_unit is
    component multiplier
        port(
            A, B : in  unsigned(7 downto 0);
            P    : out unsigned(15 downto 0)
        );
    end component;

    component multiplier16
        port(
            A, B : in  unsigned(15 downto 0);
            P    : out unsigned(31 downto 0)
        );
    end component;

----- Signals to save at first register state ---------
    signal s_started : std_logic;
    
    signal s_to_be_in_M0_a : unsigned(15 downto 0) := (others => '0');
    signal s_to_be_in_M0_b : unsigned(15 downto 0) := (others => '0');
    signal s_to_be_in_M2 : unsigned(15 downto 0) := (others => '0');

----- Signals to save at second register state ---------
    signal s_done : std_logic;

    signal s_to_be_in_A1_a : unsigned(31 downto 0) := (others => '0');
    signal s_to_be_in_A1_b : unsigned(31 downto 0) := (others => '0');

-- signals from combinatorial

    signal s_in_A0_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_A0_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_A0 : unsigned(15 downto 0) := (others => '0');

    signal s_in_A1_a : unsigned(31 downto 0) := (others => '0');
    signal s_in_A1_b : unsigned(31 downto 0) := (others => '0');
    signal s_out_A1 : unsigned(31 downto 0) := (others => '0');

    signal s_in_M1_a : unsigned(7 downto 0) := (others => '0');
    signal s_in_M1_b : unsigned(7 downto 0) := (others => '0');
    signal s_out_M1 : unsigned(15 downto 0) := (others => '0');

    signal s_in_M2_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_M2_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_M2 : unsigned(31 downto 0) := (others => '0');


    signal s_in_M0_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_M0_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_M0 : unsigned(31 downto 0) := (others => '0');

begin


	multiplier0 : multiplier16 port map(
		A => s_in_M0_a,
		B => s_in_M0_b,
		P => s_out_M0
	); 

	multiplier1 : multiplier port map(
		A => s_in_M1_a,
		B => s_in_M1_b,
		P => s_out_M1
	); 

	multiplier2 : multiplier16 port map(
		A => s_in_M2_a,
		B => s_in_M2_b,
		P => s_out_M2
	); 

update : process(clk, reset_n)
	begin
	if (reset_n='0') then
		s_done<='0';
		s_started<='0';
	elsif(rising_edge(clk)) then
		s_started <= start;
		s_done <= s_started;
		if (start = '1') then 
			-- change value the second part is working with
			s_in_M0_a <= s_to_be_in_M0_a;
			s_in_M0_b <= s_to_be_in_M0_b;
			s_in_M2_a <= s_to_be_in_M2;
			s_in_M2_b <= s_to_be_in_M2;
		end if;
		if (s_started = '1') then
			s_in_A1_a <= s_to_be_in_A1_a;
			s_in_A1_b <= s_to_be_in_A1_b;
		end if;
	end if;
end process;

s_in_A0_a <= ("0000000"&A(7 downto 0)&"0") when (sel='1') else ("00000000"&A) ;
s_in_A0_b <= "00000000"&B;
s_out_A0 <= s_in_A0_a + s_in_A0_b; -- (a+b;2a+b)

s_in_M1_a <= A when (sel='1') else B;
s_in_M1_b <= A when (sel='1') else C;
-- s_out_M1_b (b.c;a.a)

s_to_be_in_M0_a <= s_out_A0;
s_to_be_in_M0_b <= s_out_A0 when (sel='1') else s_out_M1;
-- s_out_M0 ((a+b).(b.c);(2a+b).(2a+b))

s_to_be_in_M2 <= s_out_M1;
-- s_out_M2 ((b.c).(b.c);(a.a).(a.a))

s_to_be_in_A1_a <= s_out_M0;
s_to_be_in_A1_b <= s_out_M2;
s_out_A1 <= s_in_A1_a + s_in_A1_b;
-- s_out_A1 ( (a+b).(b.c)+(b.c).(b.c);(2a+b).(2a+b) +(a.a).(a.a))

D<=s_out_A1;

done<=s_done;



end two_stage_pipeline_1;

-- =============================================================================
-- ============================ 2 STAGE PIPELINE II ============================
-- =============================================================================

-- intermediary stage before M0 and M2
-- intermediary stage in multiplier M0 and M2

-- up to 140 MHz
architecture two_stage_pipeline_2 of arith_unit is
    component multiplier
        port(
            A, B : in  unsigned(7 downto 0);
            P    : out unsigned(15 downto 0)
        );
    end component;

    component multiplier16_pipeline
        port(
            clk     : in  std_logic;
            reset_n : in  std_logic;
            A, B    : in  unsigned(15 downto 0);
            P       : out unsigned(31 downto 0)
        );
    end component;

----- Signals to save at first register state ---------
    signal s_started : std_logic;
    
    signal s_to_be_in_M0_a : unsigned(15 downto 0) := (others => '0');
    signal s_to_be_in_M0_b : unsigned(15 downto 0) := (others => '0');
    signal s_to_be_in_M2 : unsigned(15 downto 0) := (others => '0');

----- Signals to save at second register state ---------
    signal s_done : std_logic;

-- signals from combinatorial    
    signal s_in_A0_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_A0_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_A0 : unsigned(15 downto 0) := (others => '0');

    signal s_in_A1_a : unsigned(31 downto 0) := (others => '0');
    signal s_in_A1_b : unsigned(31 downto 0) := (others => '0');
    signal s_out_A1 : unsigned(31 downto 0) := (others => '0');

    signal s_in_M1_a : unsigned(7 downto 0) := (others => '0');
    signal s_in_M1_b : unsigned(7 downto 0) := (others => '0');
    signal s_out_M1 : unsigned(15 downto 0) := (others => '0');

    signal s_in_M2_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_M2_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_M2 : unsigned(31 downto 0) := (others => '0');


    signal s_in_M0_a : unsigned(15 downto 0) := (others => '0');
    signal s_in_M0_b : unsigned(15 downto 0) := (others => '0');
    signal s_out_M0 : unsigned(31 downto 0) := (others => '0');

    signal s_D : unsigned(31 downto 0) := (others => '0');

begin


	multiplier0 : multiplier16_pipeline port map(
		clk => clk,
		reset_n => reset_n,
		A => s_in_M0_a,
		B => s_in_M0_b,
		P => s_out_M0
	); 

	multiplier1 : multiplier port map(
		A => s_in_M1_a,
		B => s_in_M1_b,
		P => s_out_M1
	); 

	multiplier2 : multiplier16_pipeline port map(
		clk => clk,
		reset_n => reset_n,
		A => s_in_M2_a,
		B => s_in_M2_b,
		P => s_out_M2
	); 

update : process(clk, reset_n)
	begin
	if (reset_n='0') then
		s_done<='0';
		s_started<='0';
		s_D<=(others => '0');
	elsif(rising_edge(clk)) then
		s_started <= start;
		s_done <= s_started;
		if (start = '1') then 
			-- change value past first register stage
			s_in_M0_a <= s_to_be_in_M0_a;
			s_in_M0_b <= s_to_be_in_M0_b;
			s_in_M2_a <= s_to_be_in_M2;
			s_in_M2_b <= s_to_be_in_M2;
		end if;
		if (s_started = '1') then
			-- change value past seccond register stage
		end if;
			
	end if;
end process;

s_in_A0_a <= ("0000000"&A(7 downto 0)&"0") when (sel='1') else ("00000000"&A) ;
s_in_A0_b <= "00000000"&B;
s_out_A0 <= s_in_A0_a + s_in_A0_b; -- (a+b;2a+b)

s_in_M1_a <= A when (sel='1') else B;
s_in_M1_b <= A when (sel='1') else C;
-- s_out_M1_b (b.c;a.a)

s_to_be_in_M0_a <= s_out_A0;
s_to_be_in_M0_b <= s_out_A0 when (sel='1') else s_out_M1;
-- s_out_M0 ((a+b).(b.c);(2a+b).(2a+b))

s_to_be_in_M2 <= s_out_M1;
-- s_out_M2 ((b.c).(b.c);(a.a).(a.a))

-- s_out_A1 ( (a+b).(b.c)+(b.c).(b.c);(2a+b).(2a+b) +(a.a).(a.a))

done<=s_done;

D <= s_out_M2 + s_out_M0 when (s_done ='1') else (others => '0');



end two_stage_pipeline_2;
