library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PC is
    port(
        clk     : in  std_logic;
        reset_n : in  std_logic;
        sel_a   : in  std_logic;
        sel_imm : in  std_logic;
	branch  : in  std_logic;
        a       : in  std_logic_vector(15 downto 0);
	d_imm   : in std_logic_vector(15 downto 0);
	e_imm   : in std_logic_vector(15 downto 0);
	pc_addr : in std_logic_vector(15 downto 0);
        addr    : out std_logic_vector(15 downto 0);
	next_addr    : out std_logic_vector(15 downto 0)
    );
end PC;

architecture synth of PC is
	SIGNAL s_addr :std_logic_vector(15 downto 0) := (others=>'0');
	SIGNAL s_next_addr :std_logic_vector(15 downto 0) := (others=>'0');
	SIGNAL s_addr00 :std_logic_vector(15 downto 0) := (others=>'0');
	SIGNAL s_addr01 :std_logic_vector(15 downto 0) := (others=>'0');
	SIGNAL s_addr10 :std_logic_vector(15 downto 0) := (others=>'0');
	SIGNAL s_select : std_logic_vector(1 downto 0) := (others=>'0');
	
begin

update: process(clk,reset_n) is
begin

	if(reset_n='0') then
		s_next_addr<=(others=>'0');
	
	elsif (rising_edge(clk) AND reset_n='1') then
		
		s_next_addr <= s_addr; 
	end if;

end process;

s_addr00 <=std_logic_vector(unsigned(s_next_addr) + X"0004") when branch='0'
	else std_logic_vector( unsigned(pc_addr) + (unsigned(e_imm) + X"0004") );
s_addr01<=std_logic_vector(X"0004" + unsigned(a));
s_addr10 <= d_imm(13 downto 0)&"00";

s_select <= sel_imm & sel_a;

with (s_select) select
s_addr <=
s_addr00 when "00",
s_addr01 when "01",
s_addr10 when "10",
(others => '0') when others;

addr <= s_addr;
next_addr <= s_next_addr;
	

end synth;
