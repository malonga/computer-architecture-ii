library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity pipeline_reg_FD is
    port(
        clk           : in  std_logic;
        reset_n       : in  std_logic;
        I_rddata_in   : in  std_logic_vector(31 downto 0);
        next_addr_in  : in  std_logic_vector(15 downto 0);
        I_rddata_out  : out std_logic_vector(31 downto 0);
        next_addr_out : out std_logic_vector(15 downto 0)
    );
end pipeline_reg_FD;

architecture synth of pipeline_reg_FD is

signal s_next_addr_out : std_logic_vector(15 downto 0) := (others => '0');
signal s_I_rddata_out : std_logic_vector(31 downto 0) := (others => '0');

begin
update : Process(clk,reset_n)
begin
	if (reset_n = '0') then
		s_next_addr_out <= (others => '0');
		s_I_rddata_out<= (others => '0');
	elsif (rising_edge(clk)) then
		s_I_rddata_out <= I_rddata_in;
		s_next_addr_out <= next_addr_in; 
	end if;

end process;
next_addr_out <= s_next_addr_out;
I_rddata_out <= s_I_rddata_out;
end synth;
