library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity pipeline_reg_DE is
    port(
        clk           : in  std_logic;
        reset_n       : in  std_logic;
        a_in          : in  std_logic_vector(31 downto 0);
        b_in          : in  std_logic_vector(31 downto 0);
        d_imm_in      : in  std_logic_vector(31 downto 0);
        sel_b_in      : in  std_logic;
        op_alu_in     : in  std_logic_vector(5 downto 0);
        read_in       : in  std_logic;
        write_in      : in  std_logic;
        sel_pc_in     : in  std_logic;
        branch_op_in  : in  std_logic;
        sel_mem_in    : in  std_logic;
        rf_wren_in    : in  std_logic;
        mux_in        : in  std_logic_vector(4 downto 0);
        next_addr_in  : in  std_logic_vector(15 downto 0);
        a_out         : out std_logic_vector(31 downto 0);
        b_out         : out std_logic_vector(31 downto 0);
        d_imm_out     : out std_logic_vector(31 downto 0);
        sel_b_out     : out std_logic;
        op_alu_out    : out std_logic_vector(5 downto 0);
        read_out      : out std_logic;
        write_out     : out std_logic;
        sel_pc_out    : out std_logic;
        branch_op_out : out std_logic;
        sel_mem_out   : out std_logic;
        rf_wren_out   : out std_logic;
        mux_out       : out std_logic_vector(4 downto 0);
        next_addr_out : out std_logic_vector(15 downto 0)
    );
end pipeline_reg_DE;

architecture synth of pipeline_reg_DE is
	signal s_a_out         : std_logic_vector(31 downto 0) := (others => '0');
        signal s_b_out         : std_logic_vector(31 downto 0):= (others => '0');
        signal s_d_imm_out     : std_logic_vector(31 downto 0):= (others => '0');
        signal s_sel_b_out     : std_logic := '0';
        signal s_op_alu_out    : std_logic_vector(5 downto 0):= (others => '0');
        signal s_read_out      : std_logic:= '0';
        signal s_write_out     : std_logic:= '0';
        signal s_sel_pc_out    : std_logic:= '0';
        signal s_branch_op_out : std_logic:= '0';
        signal s_sel_mem_out   : std_logic:= '0';
        signal s_rf_wren_out   : std_logic:= '0';
        signal s_mux_out       : std_logic_vector(4 downto 0):= (others => '0');
        signal s_next_addr_out : std_logic_vector(15 downto 0):= (others => '0');
begin

update : process(clk)
	begin
	if (reset_n='0') then
		s_a_out         <= (others => '0'); --
        	s_b_out         <= (others => '0'); --
        	s_d_imm_out     <= (others => '0'); --
        	s_sel_b_out     <= '0'; -- 
        	s_op_alu_out    <= (others => '0'); -- 
        	s_read_out      <= '0'; --
        	s_write_out     <= '0'; --
        	s_sel_pc_out    <= '0'; --
        	s_branch_op_out <= '0'; --
        	s_sel_mem_out   <= '0'; --
        	s_rf_wren_out   <= '0'; --
        	s_mux_out       <= (others => '0');
        	s_next_addr_out <= (others => '0'); --
	elsif (rising_edge(clk)) then
		s_a_out <= a_in;
		s_b_out <= b_in;
		s_rf_wren_out <= rf_wren_in;
		s_sel_mem_out <= sel_mem_in;
		s_sel_pc_out <= sel_pc_in;
		s_sel_b_out <= sel_b_in;
		s_op_alu_out <= op_alu_in;
		s_read_out <= read_in;
		s_write_out <= write_in;
		s_d_imm_out <= d_imm_in;
		s_next_addr_out <= next_addr_in;
		s_branch_op_out <= branch_op_in;
		s_mux_out <= mux_in;

	end if;
end process;

a_out <= s_a_out;
b_out <= s_b_out;
rf_wren_out <= s_rf_wren_out;
sel_mem_out <= s_sel_mem_out;
sel_pc_out <= s_sel_pc_out;
sel_b_out <= s_sel_b_out;
op_alu_out  <= s_op_alu_out;
read_out  <= s_read_out;
write_out  <= s_write_out;
d_imm_out <= s_d_imm_out;
next_addr_out <= s_next_addr_out;
branch_op_out <= s_branch_op_out;
mux_out <= s_mux_out;



end synth;
