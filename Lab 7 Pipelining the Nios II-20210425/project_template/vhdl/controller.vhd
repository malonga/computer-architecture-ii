library ieee;
use ieee.std_logic_1164.all;

entity controller is
    port(
        -- instruction opcode
        op         : in  std_logic_vector(5 downto 0);
        opx        : in  std_logic_vector(5 downto 0);
        -- activates branch condition
        branch_op  : out std_logic;
        -- immediate value sign extention
        imm_signed : out std_logic;
        -- PC control signals
        pc_sel_a   : out std_logic;
        pc_sel_imm : out std_logic;
        -- register file enable
        rf_wren    : out std_logic;
	rf_retaddr : out std_logic_vector(4 downto 0);
        -- multiplexers selections
        sel_b      : out std_logic;
        sel_mem    : out std_logic;
        sel_pc     : out std_logic;
        sel_ra     : out std_logic;
        sel_rC     : out std_logic;
        -- write memory output
        read       : out std_logic;
        write      : out std_logic;
        -- alu op
        op_alu     : out std_logic_vector(5 downto 0)
    );
end controller;

architecture synth of controller is
TYPE instruction_types is (ri, jump, jumpi,call, callr, break, r, load, store, branch, iu, i);
-- SUBTYPE Execute_states IS State RANGE 4 TO 10/11 should we include both Loads, only the one that follows decode or only the one which precedes Fetch1 2345
Signal instruction_type : instruction_types := break;

signal i_op_alu:std_logic_vector(5 downto 0);
signal iu_op_alu:std_logic_vector(5 downto 0);
signal r_op_alu:std_logic_vector(5 downto 0);
signal ri_op_alu:std_logic_vector(5 downto 0);
signal br_op_alu:std_logic_vector(5 downto 0);
signal s_op_alu:std_logic_vector(5 downto 0);
signal s_up_op_alu : std_logic;


signal in_execute_state : std_logic;
signal is_R_type : std_logic;

begin

i_op_alu(2 downto 0) <= op(5 downto 3);
iu_op_alu(2 downto 0) <= op(5 downto 3);
br_op_alu(2 downto 0) <= "100" when op="000110" else op(5 downto 3);

r_op_alu(2 downto 0) <= opx(5 downto 3);
ri_op_alu(2 downto 0) <= opx(5 downto 3);


i_op_alu(5 downto 3) <= 
	"011" when op(2 downto 0)="000" else
	"000" when op(5 downto 3)="000" else
	"100";
	
iu_op_alu(5 downto 3) <=
 "011" when op(5)='1' else
   "10-";

br_op_alu(5 downto 3) <= "011" when op="000110" else "011";

with opx(2 downto 0) select r_op_alu(5 downto 3) <=
	"110" when "011",
	"100" when "110",
	"011" when "000",
	"00"&opx(3) when others;
	
ri_op_alu(5 downto 3) <= "110";

-- op_alu<=s_op_alu;



update_type: Process (op, opx) is
begin 
	if (op="111010") then
				-- R_type
				if opx(2 downto 0)="010" then
					s_op_alu <= "000001";--ri_op_alu;
					instruction_type <= ri;
				elsif (opx(2 downto 0)="101" and opx(5 downto 4)="00") then
					s_op_alu <= "000000";-- jump (ret/ jump)
					instruction_type <= jump;
				elsif (opx="110100") then
					s_op_alu<= "000000";--break
					instruction_type <= break;
				elsif opx="011101" then
					s_op_alu <= "000000"; -- jumpi (callr)
					instruction_type <= callr;
				else
					s_op_alu<= "000010";--r_op_alu
					instruction_type <= r;
				end if;
	-- I_TYPE
	elsif (op="010111") then
				s_op_alu<= "000000"; -- load
				instruction_type <= load;
	elsif (op="010101") then
				s_op_alu<= "000000"; -- store
				instruction_type <= store;
	elsif (op(3 downto 0)="0110" OR op(3 downto 0)="1110")  then
				s_op_alu<="000100";--br_op_alu
				instruction_type <= branch;
	elsif (op="000000") then
				s_op_alu <= "000000"; -- jump(call)
				instruction_type <= call;
	elsif (op="101000")or(op="110000") OR op(3 downto 0)="1100" OR op="010100"  then
				s_op_alu <= "000011";--iu_op_alu
				instruction_type <= iu;
	elsif (op="000001") then
				s_op_alu <= "000000";-- jumpi
				instruction_type <= jumpi;
	else
				s_op_alu<= "000101";--i_op_alu
				instruction_type <= i;
	end if;	
	s_up_op_alu <= '0';
end Process;

with instruction_type select op_alu <=
	ri_op_alu when ri,
	iu_op_alu when iu,
	r_op_alu when r,
	br_op_alu when branch,
	i_op_alu when i,
	"000000" when others;	


	


--read
read <= '1' when op = "010111"
else '0';

--write
write <= '1' when op = "010101" else
'0';

--with State select read <= 
--'1' when FETCH_1,
--'1' when LOAD_1, 
--'0' when others;


--imm_signed

with instruction_type select imm_signed <= 
'1' when I,
'1' when STORE,
'1' when LOAD,
'1' when branch,
'0' when others;

--rf_wren
with instruction_type select rf_wren <=
'1' when I,
'1' when R,
'1' when RI,
'1' when IU,
'1' when LOAD,
'1' when jump,
'0' when others;

--sel_b
with instruction_type select sel_b <=
'1' when R,
'1' when BRANCH,
'0' when others;

--sel_rC
sel_rC <= '1' when instruction_type = r else
'1' when instruction_type = RI else
--'1' when State=CALLR else
'0';


--sel_mem
sel_mem <= '1' when instruction_type = load else
'0';

--branch_op
branch_op <= '1' when instruction_type = BRANCH  else
'0';

--pc_sel_imm
pc_sel_imm <= '1' when instruction_type = jump else
'1' when instruction_type = JuMPI else
'0';

--sel_pc
sel_pc <= '1' when instruction_type = call else-- CALL else
'1' when instruction_type = CALLR else 
'0';

--sel_ra
sel_ra <= '1' when instruction_type = CALL else
'0';

--pc_sel_a
pc_sel_a <= '1' when instruction_type = CALLR else
'1' when instruction_type = jump else
--'1' when State = JMPI else
'0';

rf_retaddr <= "11111";--31


end synth;
