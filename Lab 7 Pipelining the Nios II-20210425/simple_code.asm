.equ    RAM, 0x1000
.equ    LEDs, 0x2000
.equ    TIMER, 0x2020
.equ    BUTTON, 0x2030
.equ    COUNTER, 0x1008
.equ    MODE, 0x100C

.equ main_mode, 0x0
.equ interrupt_mode, 0x1

.equ    LFSR, RAM

br main
nop
nop
nop
nop


main : 
addi sp, zero, LEDs  ; initiate sp
nop
nop
nop
nop

stw zero, COUNTER(zero) ; inititate counter
nop
nop
nop
nop

infinite_loop:
ldw t0, MODE(zero) ; t0 = mode
nop
nop
nop
nop

addi t1, zero, 1 ; t1 = 1
nop
nop
nop
nop

slli t0, t0, 1 ; t0 = 2*mode
nop
nop
nop
nop

sub t0, t1, t0 ; t0 = 1 | -1
nop
nop
nop
nop

ldw t1, COUNTER(zero) ; t0 = counter
nop
nop
nop
nop

add t1, t1, t0 ; t1 = counter + (1|-1)
nop
nop
nop
nop

stw t1, COUNTER(zero) ; counter <= t1
nop
nop
nop
nop

stw t1, LEDs(zero) ; LEDS <= t1
nop
nop
nop
nop


call polling
nop
nop
nop
nop


br infinite_loop
nop
nop
nop
nop

polling: 
ldw t0, BUTTON+4(zero) ; read edge_capture
nop
nop
nop
nop

addi t1, zero, 1 ; t1 = 1
nop
nop
nop
nop

and t0, t0, t1 ; t0 = if b0 is pressed
nop
nop
nop
nop

beq t0, zero, done ; stay if pressed
nop
nop
nop
nop

ldw t0, MODE(zero) ; t0 = mode
nop
nop
nop
nop

addi t1, zero, 1 ; t1 = 1
nop
nop
nop
nop

sub t0, t1, t0 ; t0 = 1 | 0
nop
nop
nop
nop

stw t0, MODE(zero) ; MODE <= t0
nop
nop
nop
nop


done:
stw zero, BUTTON+4(zero) ; clear edge_aptire
nop
nop
nop
nop

ret ; done with polling
nop
nop
nop
nop

