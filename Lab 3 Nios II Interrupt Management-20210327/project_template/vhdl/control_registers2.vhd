library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity control_registers is
    port(
        clk       : in  std_logic;
        reset_n   : in  std_logic;
        write_n   : in  std_logic;
        backup_n  : in  std_logic;
        restore_n : in  std_logic;
        address   : in  std_logic_vector(2 downto 0);
        irq       : in  std_logic_vector(31 downto 0);
        wrdata    : in  std_logic_vector(31 downto 0);

        ipending  : out std_logic;
        rddata    : out std_logic_vector(31 downto 0)
    );
end control_registers;

architecture synth of control_registers is
	
	subtype REG is std_logic_vector(31 downto 0);
	type CTL_REGISTERS is array(0 to 7) of REG;
	signal control_registers : CTL_REGISTERS := (others=>(others=>'0'));
	signal or_reduce : std_logic := '0';

begin
control_registers(4)<=irq AND control_registers(3); -- ipending(register) = irq and ienable

rddata <= control_registers(to_integer(unsigned(address))); -- Read process
control_registers(0)(31 downto 1) <= (31 downto 1=>'0'); -- ctl0 31-1 reserved
control_registers(1)(31 downto 1) <= (31 downto 1=>'0'); -- ctl1 31-1 reserved
control_registers(2)(31 downto 1) <= (31 downto 1=>'0'); -- ctl2 31-1 reserved

-- Unused registers
control_registers(6) <= X"AAAAAAAA"; 
control_registers(7) <= X"AAAAAAAA";

or_reduce<= '0' when control_registers(4)=X"00000000" else '1';
ipending<=(or_reduce AND control_registers(0)(0)); -- ipending(bit) = (ipending(register)!=0) and PIE

update : process(clk, reset_n)

	begin
		-- asynchronous changes to signals
		if (reset_n='0') then
			control_registers<=(others=>(others=>'0'));
			
		end if;
		-- synchronous
		if (rising_edge(clk) and reset_n='1') then
			if (write_n='0') then -- write 
				control_registers(to_integer(unsigned(address)))<=wrdata; 
			elsif (backup_n='0') then -- backup
				control_registers(1)(0) <= control_registers(0)(0); -- EPIE = PIE
				control_registers(0)(0)<='0'; -- PIE = 0
			elsif (restore_n='0') then -- restore
				control_registers(0)(0) <= control_registers(1)(0); -- PIE = EPIE
			end if;

		end if;

	end process;

end synth;
