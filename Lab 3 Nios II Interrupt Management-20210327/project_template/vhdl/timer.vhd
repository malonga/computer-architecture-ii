library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity timer is
    port(
        -- bus interface
        clk     : in  std_logic;
        reset_n : in  std_logic;
        cs      : in  std_logic;
        read    : in  std_logic;
        write   : in  std_logic;
        address : in  std_logic_vector(1 downto 0);
        wrdata  : in  std_logic_vector(31 downto 0);

        irq     : out std_logic;
        rddata  : out std_logic_vector(31 downto 0)
    );
end timer;

architecture synth of timer is
	subtype REG is std_logic_vector(31 downto 0);

	signal counter : REG :=(others=>'0');
	signal period : REG :=(others=>'0');
	signal ITO : std_logic := '0';
	signal CONT : std_logic := '0';
	signal TO_b : std_logic := '0';
	signal RUN: std_logic := '0';

	signal s_irq : std_logic := '0';
	signal s_rddata : std_logic_vector(31 downto 0) := (others => '0');


	

-- Normal timer behavior
	procedure normal_timer (
	Signal CONT : in std_logic;
	Signal ITO : in std_logic;
	Signal counter : inout std_logic_vector(31 downto 0);
	Signal RUN : out std_logic;
	Signal irq : out std_logic;
	Signal time_out : out std_logic
	 )is 
	begin
		-- if RUN = '1' then -- If running
			-- "decrement"
			if (counter = X"00000000") then -- Start over
				counter <= period ;
				if (CONT='0') then -- if CONT
					RUN<='0'; -- Stop
				end if;

			else -- actual decrement
				counter <= std_logic_vector(unsigned(counter)-1); -- decrement counter
			end if;
			if (counter = X"00000000") then -- check for TO
				time_out<='1'; -- set TO
				if (ITO = '1') then
					irq<='1';
				end if;
			end if;
				
		-- end if;
	end normal_timer;






begin

rddata <= s_rddata;
irq <= TO_b and ITO;

	update : process(clk, reset_n)
	begin
		if (reset_n='0') then 
			counter<=(others=>'0');
			period<=(others=>'0');
			ITO <= '0';
			CONT <= '0';
			TO_b <= '0';
			RUN <= '0';

		elsif rising_edge(clk) then
			-- communication with processor
			-- Write			
			if(cs='1' and write = '1') then
				case address IS
					when "11" => -- Status register
						if (wrdata = X"00000000") then -- Acknowledge TO
							
							TO_b<='0';
							s_irq<='0';
						end if;
					when "10" => -- Control register
						ITO <= wrdata(1);
						CONT <= wrdata(0);
						-- set ITO and CONT
						if (wrdata(2)='1') then    -- Stop
							RUN<='0';
							-- counter <= std_logic_vector(unsigned(counter)+1);
							-- status(0) <= '0';
						elsif (wrdata(3)='1') then  -- Start
							RUN<='1';
							-- counter <= std_logic_vector(unsigned(counter)-1);
							--  status(0) <= '1';
							normal_timer(CONT, ITO, counter, RUN, s_irq, TO_b);							
						end if;
					when "01" =>  -- period
						period <= wrdata;
						counter <= wrdata;
						RUN<='0';
					when others =>
				end case;

			end if;
						
			-- Read
			if((cs='1')and(read = '1')) then 
				
				case address IS
					when "11" => s_rddata <= ((31 downto 2 => '0') & TO_b & RUN); -- status;
					when "10" => s_rddata <= ((31 downto 2 => '0') & ITO & CONT); -- control
					when "01" => s_rddata <= period; -- period
					when others => s_rddata <= counter; -- counter
				end case;
			else
				s_rddata <= (31 downto 0 => 'Z');
			end if;
			if not(cs='1' and write='1' and address="10" and wrdata(3 downto 2)="01") then -- if not in the process of stopping
				if (RUN = '1') then -- if Running
					normal_timer(CONT, ITO, counter, RUN, s_irq, TO_b);
				end if;
			end if;

		end if;
	end process;

	
	
end synth;
