library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity control_registers is
    port(
        clk       : in  std_logic;
        reset_n   : in  std_logic;
        write_n   : in  std_logic;
        backup_n  : in  std_logic;
        restore_n : in  std_logic;
        address   : in  std_logic_vector(2 downto 0);
        irq       : in  std_logic_vector(31 downto 0);
        wrdata    : in  std_logic_vector(31 downto 0);

        ipending  : out std_logic;
        rddata    : out std_logic_vector(31 downto 0)
    );
end control_registers;

architecture synth of control_registers is
	
	subtype REG is std_logic_vector(31 downto 0);
	signal PIE : std_logic := '0';
	signal EPIE : std_logic := '0';
	signal BPIE : std_logic := '0';
	signal ienable : REG := (others=>'0');
	signal ipending_reg : REG := (others=>'0');
	--signal cpuid : REG := (others=>'0');
	signal or_reduce : std_logic := '0';

begin
ipending_reg<=irq AND ienable; -- ipending(register) = irq and ienable

with address select 
	rddata <= 	-- Read process
	(31 downto 1 => '0')& PIE when "000",
	(31 downto 1 => '0')& EPIE when "001",
	(31 downto 1 => '0')& BPIE when "010",
	ienable when "011",
	ipending_reg when "100",
	-- cpuid when "101",
	X"00000000" when others;


or_reduce<= '0' when ipending_reg=X"00000000" else '1';
ipending<=(or_reduce AND PIE); -- ipending(bit) = (ipending(register)!=0) and PIE

update : process(clk, reset_n)

	begin
		-- asynchronous changes to signals
		if (reset_n='0') then
			PIE<='0';
			EPIE<='0';
			BPIE <= '0';
			ienable <= (31 downto 0 => '0');
			-- cpuid <= (31 downto 0 => '0');			
			
		
		-- synchronous
		elsif (rising_edge(clk) and reset_n='1') then
			if (write_n='0') then -- write 
				case address is
					when "000" => -- ctl0
						if (wrdata=X"00000001") then
							PIE<='1';
						elsif (wrdata=X"00000000") then
							PIE<='0';
						end if;
					-- ctl1 EPIE not writable
					when "010" => -- ctl2
						BPIE<=wrdata(0);
					when "011" => -- ctl3 ienable
						ienable <= wrdata;
					-- ctl4 ipending not writable
					when "101" => -- cpuid
						--cpuid <= wrdata;
					when others =>
				end case;

			elsif (backup_n='0') then -- backup
				EPIE<=PIE;
				PIE<='0';
				
			elsif (restore_n='0') then -- restore
				PIE<=EPIE;
			end if;

		end if;

	end process;

end synth;
