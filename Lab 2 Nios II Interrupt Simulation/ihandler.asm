; ISR

; Provided address
.equ TIMER, 0x2020
.equ LEDS, 0x2000
.equ RAM, 0x1000
.equ UART, 0x2010
.equ BUTTONS, 0x2030
; Global variables addresses
.equ COUNTER_1, 0x2000
.equ COUNTER_2, 0x2004
.equ COUNTER_3, 0x2008



_start:
br main ; jump to the main function
interrupt_handler:
; save the registers to the stack
addi sp, sp, -24
stw s0, 0(sp)
stw s1, 4(sp)
stw t0, 8(sp)
stw t1, 12(sp)
stw t2, 16(sp)
stw ra, 20(sp)

wrctl ctl0, zero ; Disable processor interrupts
rdctl s0, ctl4 ; s0 running copy of ipending
rdctl s1, ctl3 ; s1 constant copy of ienable before jump to interrupt handler
wrctl ctl3, zero

; read the ipending register to identify the source
; call the corresponding routine
addi t0, zero, 1
and t0, s0, t0 ; 1 if irq0 is pending
beq t0, zero, irq0_done
; irq0 treatment
; timer interruption --> increment_counter2
call increment_counter2

irq0_done:
srli s0, s0, 1
addi t0, zero, 1
and t0, s0, t0 ; 1 if irq1 is pending
beq t0, zero, irq1_done

; irq1 treatment
; UART no treatment for now

irq1_done:
srli s0, s0, 1
addi t0, zero, 1
and t0, s0, t0 ; 1 if irq2 is pending
beq t0, zero, irq2_done

; irq2 treatment
; buttons are pressed --> increment_counter1 accordingly
call isr_2

irq2_done:

wrctl ctl4, zero ;  no more pending interrupts
wrctl ctl3, s1 ; reenable external hardware interrupts

; restore the registers from the stack
ldw s0, 0(sp)
ldw s1, 4(sp)
ldw t0, 8(sp)
ldw t1, 12(sp)
ldw t2, 16(sp)
ldw ra, 20(sp)
addi sp, sp, 24


addi ea, ea, -4 ; correct the exception return address
eret ; return from exception



main:
addi sp, zero, 0x2000



; main procedure here
; set up initial value counters
stw zero, COUNTER_1(zero)
stw zero, COUNTER_2(zero)
stw zero, COUNTER_3(zero)
; set up timer
;addi t0, zero, 25000
;slli t0, t0, 11
; We want period of 1000, continuous, interrupt raising
addi t0, zero, 1000
stw t0, TIMER+4(zero) ; Timeout period = 1000
addi t0, zero, 3
stw t0, TIMER+8(zero) ; Continuous and intterupt raising
addi t0, zero, 11
stw t0, TIMER+8(zero) ; Start timer

; Allow and enable interrupts
addi t0, zero, 7
wrctl ctl3, t0 ; enable irq0, irq1 and irq2
addi t0, zero, 1
wrctl ctl0, t0

;increment_loop
increment_loop:
ldw t0, COUNTER_3(zero)
addi t0, t0, 1
stw t0, COUNTER_3(zero)
br increment_loop

increment_counter2:
ldw t0, COUNTER_2(zero)
addi t0, t0, 1
stw t0, COUNTER_2(zero)
stw zero, TIMER+0xC(zero)
ret

isr_2:
increment_counter1:
ldw t0, BUTTONS+4(zero) ; read edge_capture
addi t2, zero, 1
and t2, t0, t2 ; whether button0 is pressed
add t1, zero, t2 ; t1 --> by how much to increment Counter_1
addi t2, zero, 2
and t2, t0, t2
srli t2, t2, 1 ; -1 if button1 is pressed, 0 otherwise
sub t1, t1, t2

ldw t0, COUNTER_1(zero) ; load value of Counter_1
add t0, t0, t1 ; increment_value of Counter_1
stw t0, COUNTER_1(zero) ; Store result
clear_edge_capture:
stw zero, BUTTONS+4(zero); clear edge capture
ret


